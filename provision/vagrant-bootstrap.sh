#!/bin/bash -e


sudo apt-get update -y

echo "### install apache ###"
sudo apt-get install -y apache2

echo "### install php ###"
sudo apt-get install -y curl php7.0  libapache2-mod-php7.0 php7.0-cli php7.0-dom php7.0-mbstring


echo "### config apache ###"
sudo a2enmod rewrite
sudo cp /var/www/provision/apache_config /etc/apache2/sites-available/000-default.conf
sudo service apache2 restart

echo "### install composer"
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

cd /var/www

composer install

echo "Done"