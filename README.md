### Installation
 
``
  vagrant up 
  
`` 

open http://localhost:8001/

If you don't have vagrant then check our provision/vagrant-bootstrap.sh file
 
 
### Tests

``
  composer exec phpunit tests/
``