<?php

namespace App\Store;


class IngredientStore extends AbstractJsonStore
{

    protected $rootKey = 'ingredients';

    /**
     * @inheritdoc
     */
    public function find($options = [])
    {
        $items = $this->fetchAll();

        $where = isset($options['where']) ? $options['where'] : [];

        // Given that an ingredient is past its use-by date, I should not receive recipes containing this ingredient
        if (!empty($where['use_by_gte'])) {
            $gt = $where['use_by_gte'];
            if (!is_numeric($gt)) {
                $gt = strtotime($gt);
            }

            $items = array_filter($items, function ($item) use ($gt) {
                return strtotime($item['use-by']) >= $gt;
            });
        }

        return $items;
    }
}