<?php

namespace App\Store;


class RecipeStore extends AbstractJsonStore
{

    protected $rootKey = 'recipes';

    /**
     * @inheritdoc
     */
    public function find($options = [])
    {
        $items = $this->fetchAll();


        // Given that an ingredient is past its best-before date, but is still within its use-by date, any
        // recipe containing this ingredient should be sorted to the bottom of the response object

        return $items;
    }
}