<?php

namespace App\Store;

use Exception;

abstract class AbstractJsonStore
{

    /**
     * Path to JSON file
     * @var string
     */
    protected $basePath;

    protected $rootKey;

    public function __construct($basePath)
    {
        $this->basePath = $basePath;
        if (!file_exists($this->basePath)) {
            throw new Exception("JSON file does not exist. Check service config please");
        }

        if (empty($this->rootKey)) {
            throw new Exception("JSON root key must be specified");
        }
    }

    /**
     * Fetch all the data from store
     * @return mixed
     * @throws Exception
     */
    protected function fetchAll()
    {
        $recipes = json_decode(file_get_contents($this->basePath), true);
        if (!isset($recipes[$this->rootKey])) {
            throw new Exception("JSON file has invalid format");
        }

        return $recipes[$this->rootKey];
    }

    /**
     * Find items according to provided options
     * @return array
     */
    abstract public function find($options = []);
}