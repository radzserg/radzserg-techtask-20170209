<?php

namespace App\Store;

use Exception;

class JsonStore
{

    /**
     * Path to JSON file
     * @var string
     */
    protected $basePath;

    /**
     * Root key in JSON file
     * @var string
     */
    protected $rootKey;

    public function __construct($basePath)
    {
        $this->basePath = $basePath;
    }

    /**
     * Fetch all the data from store
     * @return mixed
     * @throws Exception
     */
    public function fetchAll($key)
    {
        $path = $this->basePath . '/' . $key . '.json';
        if (!file_exists($path)) {
            throw new Exception("JSON file {$path} does not exist. Check service config please");
        }

        $items = json_decode(file_get_contents($path), true);
        if (!isset($items[$key])) {
            throw new Exception("JSON file has invalid format");
        }

        return $items[$key];
    }
}