<?php

namespace App\ItemFilter;


class RecipeFilter
{

    /**
     * @inheritdoc
     */
    public static function filter($allRecipes, $allIngredients, $options = [])
    {
        $ingredients = static::filterIngredients($allIngredients, $options);

        $recipes = array_filter($allRecipes, function ($recipe) use ($ingredients) {
            $recipeIngredients = $recipe['ingredients'];
            $leave = true;
            foreach ($recipeIngredients as $ingredient) {
                if (!isset($ingredients[$ingredient])) {
                    $leave = false;
                    break;
                }
            }
            return $leave;
        });

        if (!empty($options['respect_best_before'])) {
            usort($recipes, function ($a, $b) use ($ingredients) {
                $aRecipeIngredients = $a['ingredients'];
                $aMin = null;
                foreach ($aRecipeIngredients as $ingredientName) {
                    $ingredient = $ingredients[$ingredientName];
                    if (is_null($aMin)) {
                        $aMin = $ingredient['best-before'];
                    } else {
                        $aMin = min($aMin, $ingredient['best-before']);
                    }
                }

                $bMin = null;
                $bRecipeIngredients = $b['ingredients'];
                foreach ($bRecipeIngredients as $ingredientName) {
                    $ingredient = $ingredients[$ingredientName];
                    if (is_null($bMin)) {
                        $bMin = $ingredient['best-before'];
                    } else {
                        $bMin = min($bMin, $ingredient['best-before']);
                    }

                }

                return $aMin < $bMin;
            });
        }

        return $recipes;
    }

    /**
     * Filter ingredients for recipes
     * @param $allIngredients
     * @param $options
     * @return array
     */
    private static function filterIngredients($allIngredients, $options)
    {
        $ingredientsOptions = ['where' => [], 'order' => []];
        if (!empty($options['use_by_gte'])) {
            $ingredientsOptions['use_by_gte'] = $options['use_by_gte'];
        }

        $ingredients = [];
        foreach (IngredientFilter::filter($allIngredients, $ingredientsOptions) as $ingredient) {
            $ingredients[$ingredient['title']] = $ingredient;
        }
        return $ingredients;
    }
}