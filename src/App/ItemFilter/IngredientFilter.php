<?php

namespace App\ItemFilter;


class IngredientFilter
{

    /**
     * @inheritdoc
     */
    public static function filter($items, $options = [])
    {
        // Given that an ingredient is past its use-by date, I should not receive recipes containing this ingredient
        if (!empty($options['use_by_gte'])) {
            $gt = $options['use_by_gte'];
            if (!is_numeric($gt)) {
                $gt = strtotime($gt);
            }

            $items = array_filter($items, function ($item) use ($gt) {
                return strtotime($item['use-by']) >= $gt;
            });
        }


        return $items;
    }
}