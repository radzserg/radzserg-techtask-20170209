<?php

use App\ItemFilter\RecipeFilter;
use Symfony\Component\HttpFoundation\Request;

//Request::setTrustedProxies(array('127.0.0.1'));

$app->get('/', function () use ($app) {
    return $app['twig']->render('index.html.twig', array());
})->bind('homepage');


$app->get('/lunch', function () use ($app) {
    /** @var \App\Store\JsonStore $jsonStore */
    $jsonStore = $app['json_store'];
    // default search settings
    $options = [
        'use_by_gte' => time(), // strtotime('2017-02-08')
        'respect_best_before' => true,
    ];
    $recipes = RecipeFilter::filter($jsonStore->fetchAll('recipes'),
        $jsonStore->fetchAll('ingredients'),
        $options
    );

    return $app->json(['success' => true, 'recipes' => $recipes], 200);
});

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    return $app->json(['success' => false, 'error' => $e->getMessage()], $code);
});