<?php


use App\Store\JsonStore;
use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\TwigServiceProvider;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
/*
$app['twig'] = $app->extend('twig', function ($twig, $app) {
    // add custom globals, filters, tags, ...

    return $twig;
});*/


$app['paths.base'] = realpath(__DIR__ . '/../');
$app['paths.data'] = realpath($app['paths.base'] . '/var/data');

$app['json_store'] = function ($app) {
    return new JsonStore($app['paths.data']);
};

return $app;
