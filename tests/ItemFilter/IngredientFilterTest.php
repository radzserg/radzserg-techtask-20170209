<?php

use App\ItemFilter\IngredientFilter;

class IngredientFilterTest extends \PHPUnit_Framework_TestCase
{


    public function testGetHomepage()
    {
        $ingredientFilter = new IngredientFilter();

        $data = [
            [
                "title" => "Ham",
                "use-by" => "2017-02-25"
            ],
            [
                "title" => "Cheese",
                "use-by" => "2017-02-13"
            ],
            [
                "title" => "Bread",
                "use-by" => "2017-02-27"
            ],
        ];

        $gt = strtotime('2017-02-25');
        $items = $ingredientFilter->filter($data, [
            'use_by_gte' => $gt
        ]);

        $this->assertCount(2, $items);

        foreach ($items as $item) {
            $this->assertTrue(strtotime($item['use-by']) >= $gt);
        }
    }


}
