<?php


use App\ItemFilter\RecipeFilter;
use App\Store\JsonStore;

class RecipeStoreTest extends \PHPUnit_Framework_TestCase
{


    public function testGetHomepage()
    {
        $basePath = realpath(__DIR__ . '/../../var/data');
        $store = new JsonStore($basePath);
        $recipeFilter = new RecipeFilter();


        $gt = strtotime('2017-02-25');

        $ingredients = [
            [
                "title" => "Ham",
                "best-before" => "2017-02-26",
                "use-by" => "2017-02-27"
            ],
            [
                "title" => "Eggs",
                "best-before" => "2017-02-24",
                "use-by" => "2017-02-27"
            ],
            [
                "title" => "Cheese",
                "best-before" => "2017-02-08",
                "use-by" => "2017-02-13"
            ],
            [
                "title" => "Bread",
                "best-before" => "2017-02-26",
                "use-by" => "2017-02-27"
            ],
        ];

        $recipes = [
            [
                "title" => "Fry-up",
                "ingredients" => [
                    "Ham",
                    "Cheese",
                ]
            ],
            [
                "title" => "Salad",
                "ingredients" => [
                    "Ham",
                    "Bread"
                ]
            ],
            [
                "title" => "Hotdog",
                "ingredients" => [
                    "Eggs",
                    "Bread"
                ]
            ],
        ];


        $recipes = $recipeFilter->filter($recipes, $ingredients, [
            'use_by_gte' => $gt,
            'respect_best_before' => true,
        ]);

        $this->assertCount(2, $recipes);

        $titles = [];
        foreach ($recipes as $recipe) {
            $titles[] = $recipe['title'];
        }
        $this->assertEquals(['Salad', 'Hotdog'], $titles);
    }


}
