<?php

use Silex\WebTestCase;

class controllersTest extends WebTestCase
{
    public function testGetHomepage()
    {
        $client = $this->createClient();
        $client->followRedirects(true);
        $crawler = $client->request('GET', '/');

        $this->assertTrue($client->getResponse()->isOk());
        $this->assertContains('Manage flitter test app', $crawler->filter('body')->text());
    }

    public function testLunchPage()
    {
        $client = $this->createClient();
        $client->request('GET', '/lunch');

        $response = $client->getResponse();
        $this->assertTrue($response->isOk());

        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));
        $content = json_decode($client->getResponse()->getContent(), true);

        $this->assertTrue($content['success']);
        $this->assertNotEmpty($content['recipes']);
        $recipe = $content['recipes'][0];
        $this->assertArrayHasKey('title', $recipe);
        $this->assertArrayHasKey('ingredients', $recipe);
    }

    public function testErrorPage()
    {
        $client = $this->createClient();
        $client->request('GET', '/dummy');
        $this->assertTrue($client->getResponse()->isNotFound());

        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertFalse($content['success']);
        $this->assertContains('No route found for', $content['error']);
    }


    public function createApplication()
    {
        $app = require __DIR__.'/../src/app.php';
        require __DIR__.'/../config/dev.php';
        require __DIR__.'/../src/controllers.php';
        $app['session.test'] = true;

        return $this->app = $app;
    }
}
